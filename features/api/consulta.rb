require 'httparty'
require 'json'
require 'pry'


class Consulta
    def consultar_cep(cep)
        response = HTTParty.get("https://viacep.com.br/ws/#{cep}/json",
            :headers => {'Authorization' => 'application/json'})
        if response.code != 200
            raise "API não funcionou. Código: #{response.code.to_s}"
        else 
            response
        end    
    end
end